﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class Inode : ScriptableObject
{
    [MenuItem("Tools/MyTool/Do It in C#")]
    static void DoIt()
    {
        EditorUtility.DisplayDialog("MyTool", "Do It in C# !", "OK", "");
    }

    public class InoNode
    {
        public string name;
        public List<InoNode> children = new List<InoNode>();
    }

    InoNode root = new InoNode();

}