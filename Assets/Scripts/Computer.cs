﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Computer : MonoBehaviour
{
    public string[] dir;


    // Start is called before the first frame update
    void Start()
    {
        dir[0] = "/";
        dir[1] = "/bin";
        dir[2] = "/boot";
        dir[3] = "/etc";
        dir[4] = "/home";
        dir[5] = "/lib";
        dir[6] = "/tpm";
        dir[7] = "/var";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
